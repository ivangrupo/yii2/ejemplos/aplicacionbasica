<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        return $this->render('index');
    }

    public function actionEstamos(){
        return $this->render('estamos');
    }
    
    public function actionConocenos(){
        return $this->render('conocenos');
    }
    
    public function actionOperaciones(){
        return $this->render('operaciones');
    }
    
    /* acción por defecto de Yii 2
    
    public function actionContacto(){
        // var_dump(Yii::$app->request->get('nombre')); sólo el campo nombre
        // var_dump(Yii::$app->request->get()); todo el array
        Yii::$app->request->get();
        return $this->render('contacto',[
            'titulo' => 'Introduce tus datos',
        ]);
    }
    
     */
    
    public function actionContacto(){
        
       //var_dump(Yii::$app->request->get('nombre'));
        //var_dump($_REQUEST);
        $longitud=0;
        $datos=Yii::$app->request->get();
        $vista="contacto";
        //var_dump($datos);
        
        /**
         * conocer si he pulsado el boton de enviar y por lo tanto
         * me llegan datos
         */
        if(isset($datos['boton'])){
            $longitud=strlen($datos['nombre']);
            $vista="resultado";
            //var_dump($longitud);
        }
               
        return $this->render($vista,[
           'titulo'=>'Introduce tus datos',
            'longitud'=>$longitud,
        ]);
    }
    
    public function actionContacto1(){
        // var_dump(Yii::$app->request->get('nombre')); sólo el campo nombre
        // var_dump(Yii::$app->request->get()); todo el array
        $longitud=0;
        $datos=Yii::$app->request->get();
        
        // var_dump($datos);
        
        // Comprobar si se ha pulsado el botón y me llegan datos
        if(isset($datos['boton'])){
            $longitud=strlen($datos['nombre']);
        
            return $this->render('contacto',[
                'longitud' => $longitud,
            ]);
        }
        return $this->render('contacto',[
                'titulo' => 'Introduce tus datos',
        ]);
    }

}


/* Cambiar siempre la vista de url en config/web.php - Descomentar línea 47 a 52 y cambiar a true

'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => true,
            'rules' => [
            ],
        ],

 */