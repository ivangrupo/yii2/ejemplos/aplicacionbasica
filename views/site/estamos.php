<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = '¿Dónde estamos?';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2047.7432043351969!2d-3.813272187035792!3d43.461720126487066!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd494bcc568fd8c3%3A0x7858c552ade0effc!2sAlpe+Formacion!5e0!3m2!1ses!2ses!4v1536576271862" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

</div>
