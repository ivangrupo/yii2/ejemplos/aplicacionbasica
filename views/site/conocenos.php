<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Conócenos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::img('@web/imgs/Desarrollo de Aplicaciones con Tecnología Web.jpg', ['alt' => 'Curso Desarrollo 2018']) ?>
    </p>
    <div class="container">

        <div class="row">
            <div class="col-sm-4 col-md-3">
                <div class="thumbnail">
                    <img src="https://picsum.photos/200/100/?random=1" alt="...">
                    <div class="caption">
                        <h3>Foto 1</h3>
                        <p>Introduce un texto</p>
                        <p><a href="#" class="btn btn-primary" role="button">Button</a></p>
                    </div>
                </div>
                <div class="thumbnail">
                    <img src="https://picsum.photos/200/100/?random=2" alt="...">
                    <div class="caption">
                        <h3>Foto 2</h3>
                        <p>Introduce un texto</p>
                        <p><a href="#" class="btn btn-primary" role="button">Button</a></p>
                    </div>
                </div>
                <div class="thumbnail">
                    <img src="https://picsum.photos/200/100/?random=3" alt="...">
                    <div class="caption">
                        <h3>Foto 3</h3>
                        <p>Introduce un texto</p>
                        <p><a href="#" class="btn btn-default" role="button">Button</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
