<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contacto';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= $titulo ?></h1>

<?php
/*
  var_dump($this);

  echo "<pre>";
  yii\helpers\VarDumper::dump($this);
  echo "</pre>";
 */
?>

    <form method="get">
        <div class="form-group">
            <label for="nombre">Nombre Completo</label>
            <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Escribe tu nombre">
        </div>
        <button type="submit" name="enviar" class="btn btn-default">Enviar</button>
    </form>


</div>
