<?php
    require_once "opController.php";
    
    use yii\helpers\Html;

    $this->title = 'Operaciones matemáticas';
    $this->params['breadcrumbs'][] = $this->title;
    
?>

    <style>
        #wrapper{
            display: block;
            text-align: center;
            font-size: 1.5em;
        }
        div, label, imput, button, a{
            margin: 5px;
            text-decoration: none;
        }
        h3{
            color: red;
            margin: 20px 100px;
        }
    </style>

        <div class="site-about">
            <h1><?= Html::encode($this->title) ?></h1>
            <div>
                <h3><a href="operaciones.php">FORMULARIO</a></h3>
            </div>
            <form method="get">
                <?php
                include $datos["content"];
                ?>
            </form>

        </div>


