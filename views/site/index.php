<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Primer ejemplo de Yii 2';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Página de Inicio!</h1>

        <p class="lead">Desarrollado por I. Garrote</p>
        

        <p><a class="btn btn-lg btn-success" href="https://www.yiiframework.com/doc/api/2.0">Api Yii2 Documentación</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Manual Yii 2</h2>

                <p>Todas las clases de Yii 2</p>

                <p><a class="btn btn-default" href="https://www.yiiframework.com/doc/guide/2.0/es">Yii2 Guía &raquo;</a></p>
                <p><?= Html::a('Guía de Yii2', 'https://www.yiiframework.com/doc/guide/2.0/es', ['site/index'], ['class' => 'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Helper-HTML  Yii 2</h2>

                <p>Clases auxiliares: HTML Yii 2</p>

                <p><a class="btn btn-default" href="https://www.yiiframework.com/doc/guide/2.0/es/helper-html">Api Yii2 Documentación &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Bootstrap Yii 2</h2>

                <p>Documentación Api Bootstrap Yii 2</p>

                <p><a class="btn btn-default" href="https://www.yiiframework.com/extension/yiisoft/yii2-bootstrap/doc/api/2.1">Bootstrap Yii2 Documentación &raquo;</a></p>
                <p><?= Html::a('Guía de Yii2', 'https://www.yiiframework.com/extension/yiisoft/yii2-bootstrap/doc/api/2.1', ['site/index'], ['class' => 'btn btn-default']) ?></p>
            </div>
        </div>
        <div class="jumbotron">
            <h2>BOOTSTRAP 3</h2>

            <p class="lead">Componentes de Bootstrap</p>
        

            <p><a class="btn btn-lg btn-success" href="https://getbootstrap.com/docs/3.3/components/#navbar">Bootstrap</a></p>
        </div>

    </div>
</div>


<!-- Cambiar siempre la vista de url en config/web.php - Descomentar línea 47 a 52 y cambiar a true
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => true,
            'rules' => [
            ],
        ], -->
